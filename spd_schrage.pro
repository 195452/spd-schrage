TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    task.h

DISTFILES += \
    in100.txt \
    in200.txt \
    in07.txt \
    in50.txt

