#ifndef TASK
#define TASK

struct Task
{
    size_t r,p,q,id;
    bool operator < (const Task& str) const
    {
        return ( r  < str.r );
    }
    void operator = (const Task& str)
    {
        r=str.r;
        p=str.p;
        q=str.q;
        id=str.id;
    }
};

struct less_than_r
{
    inline bool operator() (const Task& str1, const Task& str2)
    {
        return (str1.r < str2.r);
    }
};

struct more_than_r
{
    inline bool operator() (const Task& str1, const Task& str2)
    {
        return (str1.r > str2.r);
    }
};

struct less_than_q
{
    inline bool operator() (const Task& str1, const Task& str2)
    {
        return (str1.q < str2.q);
    }
};

struct more_than_q
{
    inline bool operator() (const Task& str1, const Task& str2)
    {
        return (str1.q > str2.q);
    }
};

#endif // TASK

