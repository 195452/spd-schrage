#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <limits>
#include <iterator>

#include "task.h"

using namespace std;

vector<Task> schrage_rpq(vector<Task> zadania)
{
    vector<Task> zadania_gotowe;
    vector<Task> zadania_uszeregowane;

    make_heap(zadania.begin(),zadania.end(),more_than_r()); // kopiec min
    size_t t=zadania.front().r;

    while(!zadania.empty() || !zadania_gotowe.empty())
    {
        while(!zadania.empty() && (*zadania.begin()).r<=t)
        {
            zadania_gotowe.push_back(zadania.front());
            push_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            pop_heap(zadania.begin(),zadania.end(),more_than_r());
            zadania.pop_back();
        }
        if (zadania_gotowe.empty())
            t = zadania.front().r;
        else
        {
            zadania_uszeregowane.push_back(zadania_gotowe.front());
            pop_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            zadania_gotowe.pop_back();
            t+=zadania_uszeregowane.back().p;
        }
    }
    return zadania_uszeregowane;
}

size_t schrage_rpqm(vector<Task> zadania)
{
    vector<Task> zadania_gotowe;
    Task zadanie_l;
    size_t cmax=0;

    zadanie_l.q=numeric_limits<size_t>::max();

    make_heap(zadania.begin(),zadania.end(),more_than_r()); // kopiec min
    size_t t=zadania.front().r;

    while(!zadania.empty() || !zadania_gotowe.empty())
    {
        while(!zadania.empty() && (*zadania.begin()).r<=t)
        {
            zadania_gotowe.push_back(zadania.front());
            push_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            Task zadanie_e = zadania.front();
            pop_heap(zadania.begin(),zadania.end(),more_than_r());
            zadania.pop_back();
            if (zadanie_e.q > zadanie_l.q) //5
            {
                zadanie_l.p=t-zadanie_e.r;
                t=zadanie_e.r;
                if (zadanie_l.p>0)
                {
                    zadania_gotowe.push_back(zadanie_l);
                    push_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
                }
            }
        }
        if (zadania_gotowe.empty())
            t = zadania.front().r;
        else
        {
            zadanie_l = zadania_gotowe.front();
            t+=zadanie_l.p;
            pop_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            zadania_gotowe.pop_back();
            cmax=max(cmax,t+zadanie_l.q);
        }

    }
    return cmax;
}

vector<Task> schrage_rpq_minmax(vector<Task> zadania)
{
    vector<Task> zadania_gotowe;
    vector<Task> zadania_uszeregowane;

    vector<Task>::iterator zadanie_najmniejsze = min_element(zadania.begin(),zadania.end(),less_than_r());
    size_t t=(*zadanie_najmniejsze).r;

    while(!zadania.empty() || !zadania_gotowe.empty())
    {
        zadanie_najmniejsze = min_element(zadania.begin(),zadania.end(),less_than_r());
        while(!zadania.empty() && (*zadanie_najmniejsze).r <=t)
        {
            zadania_gotowe.push_back(*zadanie_najmniejsze);
            zadania.erase(zadanie_najmniejsze);
            zadanie_najmniejsze = min_element(zadania.begin(),zadania.end(),less_than_r());
        }
        if (zadania_gotowe.empty())
            t = (*zadanie_najmniejsze).r;
        else
        {
            vector<Task>::iterator zadanie_tmp = max_element(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            zadania_uszeregowane.push_back(*zadanie_tmp);
            zadania_gotowe.erase(zadanie_tmp);
            t+=(*zadanie_tmp).p;
        }
    }
    return zadania_uszeregowane;
}

size_t schrage_rpqm_minmax(vector<Task> zadania)
{
    vector<Task> zadania_gotowe;
    Task zadanie_l;
    size_t cmax=0;

    zadanie_l.q=numeric_limits<size_t>::max();

    vector<Task>::iterator zadanie_najmniejsze = min_element(zadania.begin(),zadania.end(),less_than_r());

    size_t t=(*zadanie_najmniejsze).r;

    while(!zadania.empty() || !zadania_gotowe.empty())
    {
        vector<Task>::iterator zadanie_najmniejsze = min_element(zadania.begin(),zadania.end(),less_than_r());

        while(!zadania.empty() && (*zadanie_najmniejsze).r<=t)
        {
            zadania_gotowe.push_back(*zadanie_najmniejsze);
            Task zadanie_e = *zadanie_najmniejsze;
            zadania.erase(zadanie_najmniejsze);
            zadanie_najmniejsze = min_element(zadania.begin(),zadania.end(),less_than_r());
            if (zadanie_e.q > zadanie_l.q) //5
            {
                zadanie_l.p=t-zadanie_e.r;
                t=zadanie_e.r;
                if (zadanie_l.p>0)
                {
                    zadania_gotowe.push_back(zadanie_l);
                }
            }
        }
        if (zadania_gotowe.empty())
            t = (*zadanie_najmniejsze).r;
        else
        {
            vector<Task>::iterator zadanie_tmp = max_element(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            zadanie_l = (*zadanie_tmp);
            t+=zadanie_l.p;
            zadania_gotowe.erase(zadanie_tmp);
            cmax=max(cmax,t+zadanie_l.q);
        }

    }
    return cmax;
}

int main()
{
    size_t i, j;
    size_t czas_suma_rpq=0;
    size_t czas_suma_rpqm=0;
    size_t czas_rpqm=0;
    ifstream input_file;
    size_t liczba_zadan;
    vector <Task> zadania;

    string pliki[3] = {"in200.txt","in100.txt","in50.txt"};

    for(j=0;j<3;j++)
    {
        zadania.clear();
        input_file.open(pliki[j].c_str(), ios::out);
        if (input_file.is_open())
        {
            input_file >> liczba_zadan;
            input_file.ignore(80,'\n');
            zadania.resize(liczba_zadan);

            for (i=0;i<liczba_zadan;i++)
            {
                zadania[i].id=i;
                input_file >> zadania[i].r;
                input_file >> zadania[i].p;
                input_file >> zadania[i].q;
            }
            input_file.close();
        }
        else
            cout << "Błąd otwierania pliku";

        zadania=schrage_rpq_minmax(zadania);
        //zadania=schrage_rpq(zadania);

        size_t czas=0;
        size_t czas_q=0;

        for(vector<Task>::const_iterator i=zadania.begin();i!=zadania.end();i++)
        {
            if((*i).r>czas)
                czas=(*i).r;
            czas+=(*i).p;
            if ((czas + (*i).q) > czas_q)
                czas_q=(czas + (*i).q);
        }

        czas_rpqm=schrage_rpqm_minmax(zadania);

        czas_suma_rpq+=czas_q;
        czas_suma_rpqm+=czas_rpqm;

        cout << "Schrage rpq, " << zadania.size() << " zadan, czas: " << czas_q << endl;
        cout << "Schrage rpqm, " << zadania.size() << " zadan, czas: " << czas_rpqm << endl;
    }
    cout << endl << "Suma Schrage rpq: " << czas_suma_rpq << endl;
    cout << "Suma Schrage rpqm: " << czas_suma_rpqm << endl;
    return 0;
}

